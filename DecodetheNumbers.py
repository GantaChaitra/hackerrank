'''
A message containing letters from A-Z is encoded to numbers using the following mapping:

'A' -> 1

'B' -> 2

.

.

.

'Z' -> 26

Write code to determine the total number of ways to decode it.

Example:

128 --> 2
It could be decoded as "L H" (12 8), or "A B H" (1 2 8).

74 --> 1

0 --> 0
Input Format

String

Constraints

Conditional Statements

Operators

Loops

Output Format

Input

Sample Input 0

128
Sample Output 0

2
Sample Input 1

74
Sample Output 1

1
Sample Input 2

0
Sample Output 2

0
'''
number = input()
count = 1
if(number == "0"):
    print(0)
else:
    for i in range(len(number)-1):
        x = number[i] + number[i+1]
        y = int(x)
        if (y <= 26):
            count += 1
    print(count)

#Output : 
#128
#2

#74
#1
