'''For the first two years, a dog year is equal to 10.5 human years. After that, each dog year equals 4 human years.

Input Format

Integer

Constraints

Conditional statements

Operators

Output Format

Integer

Sample Input 0

12
Sample Output 0

61
'''
n = int(input())
def dog(n):
    if n>0:
        sum = 21 + (n-2)*4
        return sum
    elif(n == 0):
        return 0
    else:
        return -1
print(dog(n))

#Output : 12
########  61

