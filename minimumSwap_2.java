static int minimumSwaps(int[] arr) {
        int n = arr.length - 1;
        int minSwaps = 0;
            for(int i = 0; i < n; i++) {
                if (i < arr[i] - 1) {
                    swap(arr, i, Math.min(n, arr[i] - 1));
                    minSwaps++;
                    i--;
                 }
             }
        return minSwaps;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
