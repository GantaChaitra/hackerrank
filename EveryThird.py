'''
Print and then delete every third number from a list of numbers until the list becomes empty.

Example:

[ 10, 20, 30, 40, 50, 60, 70, 80, 90] --->

30

60

90

40

80

50

20

70

10

[ 10, 20, 50]--->

50

10

20

Input Format

List

Constraints

Loop

Operator

List methods(Use pop method)

Output Format

Integers

Sample Input 0

10 20 30 40 50 60 70 80 90
Sample Output 0

30
60
90
40
80
50
20
70
10
'''


def remove_num(int_list):
    position = 3 - 1
    i = 0
    len_list = (len(int_list))
    while (len_list > 0):
        i = (position + i) % len_list
        print(int_list.pop(i))
        len_list -= 1
n = [int(a) for a in input().split()]
remove_num(n)

#output : 10 20 30 40 50 60 70 80 90
#30
#60
#90
#40
#80
#50
#20
#70
#10
