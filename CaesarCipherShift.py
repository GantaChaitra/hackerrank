'''Caesar's cipher shift is an encryption technique that works by shifting each letter in the string N places down in the alphabet. Punctuation, spaces, and capitalization should remain intact. Take the string parameter and perform a Caesar Cipher shift on it using the num parameter for shifiting the letters.

Example:

Input:

"Caesar Cipher"

2

Output:

'Ecguct Ekrjgt'

Input Format

String

Integer

Constraints

Loops Strings Conditional statements

Output Format

String

Sample Input 0

'Come'
3
Sample Output 0

'Frph'
'''

b = input()
n = int(input())
def subcipher(a,s):
    res = ""
    for i in range(len(a)):
        char = a[i]
        if char == "'":
            res += "'"
        elif char == " ":
            res += " "
        elif char == '"':
            res += '"'
        else:
            if char.isupper():
                res += chr((ord(char) + s - 65) % 26 + 65)
            else:
                res += chr((ord(char) + s -97) % 26 + 97)
    return res
print(subcipher(b,n))

#Output : 
#Come
#3

#Frph