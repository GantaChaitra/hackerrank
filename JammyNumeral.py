'''
Lucky numbers are the positive integers whose decimal representations contain only the digits 4 and 7 like 47, 744

Check if the number of lucky digits in a given number is a lucky number.

Example:

4774 contains 4 lucky digits and 4 is a lucky number

7774447 contains 7 lucky digits and 7 is a lucky number

Input Format

Integer

Constraints

Logical Operators

Output Format

Boolean

Sample Input 0

4777474
Sample Output 0

True
Sample Input 1

4704
Sample Output 1

False
Sample Input 2

7777777
Sample Output 2

True
'''


a = int(input())
count = 0
while a > 0:
    x = a % 10
    if x == 4 or x == 7:
        count += 1
    a //= 10
if count == 4 or count == 7:
    print(True)
else:
    print(False)

#Output :
#4777474
#True

#474742
#False
