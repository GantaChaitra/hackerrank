'''
For a given number 'k' generate a number pattern of k steps where every nth step consists of integers from 1 to n.

Note: No spaces between the numbers

Input Format

Integer

Constraints

Loops

Output Format

Number Pattern

Sample Input 0

5
Sample Output 0

1
12
123
1234
12345
 
'''
n = int(input())

for i in range(1,n+1):
    for j in range(1,i+1):
        print(j,end="")
    print("")


#Output : 5
1
12
123
1234
12345

